casparcg-server (2.3.3+dfsg-3) unstable; urgency=medium

  [ Filip Hanes ]
  * Added build dependencies zlib1g-dev and libicu-dev
  * Better SIMDE configuration in 1002-arm64-compatibility.patch
  * Updated Vcs-* to multimedia-team d/control
  * Added architecture i386

  [ Petter Reinholdtsen ]
  * Change package maintainer to the Debian Multimedia group.

 -- Filip Hanes <filip@hanes.tech>  Fri, 02 Sep 2022 16:30:00 +0100

casparcg-server (2.3.3+dfsg-2) unstable; urgency=medium

  * Removed unneeded build-deps
  * Limit architectures to amd64 and arm64 (Closes: #922563)

 -- Filip Hanes <filip@hanes.tech>  Tue, 31 Aug 2022 18:30:00 +0100

casparcg-server (2.3.3+dfsg-1) unstable; urgency=medium

  [Filip Hanes]
  * New upstream version 2.3.3+dfsg (Closes: #947518)
  * New maintainer (Closes: #959124)
  * Added 0001-fix-missing-thread-includes-1396.patch
  * Added 0002-ffmpeg-5.0-1419.patch (Closes: #1015780)
  * Added 0003-fix-boost-1.73-ftbfs-1397.patch
  * Added 0004-fix-shader-frag.patch
  * Added 1001-cmake-ftbfs.patch (Closes: #1011654)
  * Added 1002-arm64-compatibility.patch
  * Added missing build dependency libboost-system-dev.
  * Added build dependency libsimde-dev for arm64 compatibility
  * Remove build dependency libgconf2-dev (Closes: #959106)
  * Update Maintainer, Uploaders, Standards-Version in d/control
  * Make ffmpeg5.0 patch compatible with ffmpeg 4.3

 -- Filip Hanes <filip@hanes.tech>  Tue, 31 Aug 2022 16:35:00 +0100

casparcg-server (2.2.0+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix ftbfs with new cmake (Closes: #947518)

 -- Dimitri John Ledkov <xnox@debian.org>  Fri, 14 Feb 2020 18:34:23 +0000

casparcg-server (2.2.0+dfsg-2) unstable; urgency=medium

  * Corrected Vcs links in d/control.
  * Updated Standards-Version from 4.2.1 to 4.3.0.
  * Added missing build dependency libxrandr-dev (Closes: #919332).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 15 Jan 2019 12:22:40 +0100

casparcg-server (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.0+dfsg.
  * Removed 0000-from-upstream-git.patch now included upstream.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 30 Dec 2018 09:14:20 +0100

casparcg-server (2.2.0~beta7+dfsg-1) unstable; urgency=medium

  * Initial upload (Closes: #912774)
  * Included 0000-from-upstream-git.patch from upstream git
    to get the latest version with all copyright header fixes.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 16 Dec 2018 16:50:07 +0100
